import { NgModule } from "@angular/core";
import { RouterModule, Routes} from '@angular/router'
import { DoctorInfoComponent } from "./doctor-info/doctor-info.component";

import { DoctorComponent } from "./doctor/doctor.component";
const routes: Routes  = [
  {
    path: '',
    data: {
      title: 'Doctors'
    },
    children: [
      {
        path: '',
        redirectTo: 'select'
      }, 
      {
        path: 'select',
        component: DoctorComponent,
        data: {
          title: 'Doctors'
        }
      },
      {
        path: 'info',
        component: DoctorInfoComponent,
        data: {
          title: 'Doctors details'
        }
      }
    ]

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorsRoutingModule {}